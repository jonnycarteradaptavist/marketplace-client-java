#!/usr/bin/env bash
set -v -e

# check configuration
testVariable() { test -n "${!1}" || (echo "Environment variable '$1' not set; I'm going to exit" && exit 1) }
testVariable TAR_GZ_ARCHIVE # the tar.gz file to upload
testVariable VERSION        # version of Bamboo for which docs are to be uploaded

# clone docs repository
rm -rf docs-repository
GIT_LFS_SKIP_SMUDGE=1 git clone https://$GIT_USER_NAME:$MPCJ_2SV_PWD@bitbucket.org/atlassian/docs-atlassian-com-contents.git docs-repository

# Install Git LFS if not present
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
apt-get install git-lfs

# initialise LFS locally
cd docs-repository
git lfs install --local
cd ..

# add new content
mkdir -p "docs-repository/marketplace-client-java/${VERSION}"
cp "${TAR_GZ_ARCHIVE}" "docs-repository/marketplace-client-java/${VERSION}"

# publish changes
cd docs-repository
git add "marketplace-client-java/${VERSION}/$(basename "${TAR_GZ_ARCHIVE}")"
git commit -m "Uploading $(basename "${TAR_GZ_ARCHIVE}")"
git tag -a `date +release_%Y%m%d_%H%M%S` -m "tagging release $VERSION"
git push -u origin master
