#!/usr/bin/env bash
set -v -e

SOURCE_DIR=Javadocs

# check configuration
testVariable() { test -n "${!1}" || (echo "ERROR: Environment variable '$1' not set; I'm going to exit" && exit 1) }
testVariable TAR_GZ_ARCHIVE # the tar.gz file to create
testVariable DOCS_DIRECTORY # relative path to the directory where docs will be pu

# sanity check - build artifacts must exists
test -d "${SOURCE_DIR}" || (echo "ERROR: ${SOURCE_DIR} is not a directory. I'm going to exit" && exit 1)

# create directory
DOCS_DIRECTORY_ROOT="${DOCS_DIRECTORY%%/*}"
rm -rf "${DOCS_DIRECTORY_ROOT}"
mkdir -p "${DOCS_DIRECTORY}"

# copy apidocs to DOCS_DIRECTORY
cp -rf "${SOURCE_DIR}/." "${DOCS_DIRECTORY}/"

# create archive to upload
tar zcvf "${TAR_GZ_ARCHIVE}" "${DOCS_DIRECTORY_ROOT}"
