package com.atlassian.marketplace.client.api;

import static com.atlassian.marketplace.client.api.QueryProperties.describeOptBoolean;
import static com.atlassian.marketplace.client.api.QueryProperties.describeParams;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Encapsulates search parameters that can be passed to {@link Vendors} to determine what
 * subset of vendors you are interested in.  Besides the usual {@link QueryBounds} parameters,
 * you can specify whether to query all vendors (the default) or only the vendors that are
 * associated with the current authenticated user.
 * @since 2.0.0
 */
public final class VendorQuery implements QueryProperties.Bounds
{
    private static final VendorQuery DEFAULT_QUERY = builder().build();
    
    private final QueryBounds bounds;
    private final boolean forThisUserOnly;
    
    /**
     * Returns a new {@link Builder} for constructing a VendorQuery.
     */
    public static Builder builder()
    {
        return new Builder();
    }

    /**
     * Returns a VendorQuery with default criteria.
     */
    public static VendorQuery any()
    {
        return DEFAULT_QUERY;
    }
    
    /**
     * Returns a new {@link Builder} for constructing a VendorQuery based on an existing VendorQuery.
     */
    public static Builder builder(VendorQuery query)
    {
        return builder()
            .bounds(query.getBounds())
            .forThisUserOnly(query.isForThisUserOnly());
    }

    private VendorQuery(Builder builder)
    {
        bounds = builder.bounds;
        forThisUserOnly = builder.forThisUserOnly;
    }
    
    @Override
    public QueryBounds getBounds()
    {
        return bounds;
    }
    
    /**
     * True if the client is querying only the list of vendors that are associated with the
     * current authenticated user.
     * @see Builder#forThisUserOnly(boolean)
     */
    public boolean isForThisUserOnly()
    {
        return forThisUserOnly;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String toString()
    {
        return describeParams("VendorQuery",
            describeOptBoolean("forThisUserOnly", forThisUserOnly),
            bounds.describe()
        );
    }
    
    @Override
    public boolean equals(Object other)
    {
        return (other instanceof VendorQuery) ? toString().equals(other.toString()) : false;
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }

    /**
     * Builder class for {@link VendorQuery}.  Use {@link VendorQuery#builder()} to create an instance. 
     */
    public static class Builder implements QueryBuilderProperties.Bounds<Builder>
    {
        private QueryBounds bounds = QueryBounds.defaultBounds();
        private boolean forThisUserOnly;
        
        /**
         * Returns an immutable {@link VendorQuery} based on the current builder properties.
         */
        public VendorQuery build()
        {
            return new VendorQuery(this);
        }
        
        @Override
        public Builder bounds(QueryBounds bounds)
        {
            this.bounds = checkNotNull(bounds);
            return this;
        }
        
        /**
         * Specifies whether to query only the vendor(s) associated with the current authenticated user
         * or all vendors.
         * @param forThisUserOnly  true to query only the vendor(s) associated with the current
         *   authenticated user; false (the default) to query all vendors.
         * @return  the same query builder
         * @see VendorQuery#isForThisUserOnly()
         */
        public Builder forThisUserOnly(boolean forThisUserOnly)
        {
            this.forThisUserOnly = forThisUserOnly;
            return this;
        }
    }
}
